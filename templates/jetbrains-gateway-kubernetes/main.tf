locals {
  # replace("1 + 2 + 3", "+", "-")
  folder_name = replace(try(element(split("/", data.coder_parameter.repo_url.value), length(split("/", data.coder_parameter.repo_url.value)) - 1), ""), ".git", "")
}

module "gitauth_options" {
  count = var.enable_git_auth ? 1 : 0

  source = "./modules/gitauth"

  gitlab_auth_id = var.gitlab_auth_id
}

# ===== Coder Agents =====
resource "coder_agent" "main" {
  arch                    = data.coder_provisioner.me.arch
  os                      = "linux"
  dir                     = "/home/coder/workspaces/${local.folder_name}"
  startup_script_behavior = "blocking"
  startup_script          = <<-EOT
    export SOURCE_FOLDER=/home/coder/workspaces/${local.folder_name}

    # Check if the directory is empty
    # and if it is, clone the repo, otherwise skip cloning
    if test -z "${data.coder_parameter.repo_url.value}" 
    then
      echo "No git repo specified, skipping"
      exit 0
    else
      if [ ! -d "/home/coder/workspaces/${local.folder_name}" ] 
      then
        if [ -z "$(ls -A "$SOURCE_FOLDER")" ]; then
          echo "Cloning ${data.coder_parameter.repo_url.value} to $SOURCE_FOLDER..."
          git clone ${data.coder_parameter.repo_url.value} --branch ${data.coder_parameter.repo_branch.value} $SOURCE_FOLDER
        else
          echo "$SOURCE_FOLDER already exists and isn't empty, skipping clone!"
          exit 0
        fi
      fi
    fi

    # start code-server
    /home/coder/.code-server/bin/code-server --auth none --port 13337 >/tmp/code-server.log 2>&1 &

    # script to symlink JetBrains Gateway IDE directory to image-installed IDE directory
    # More info: https://www.jetbrains.com/help/idea/remote-development-troubleshooting.html#setup
    cd /home/coder/.ide/bin
    ./remote-dev-server.sh registerBackendLocationForGateway >/dev/null 2>&1 &
  EOT

  display_apps {
    vscode          = false
    vscode_insiders = false
    web_terminal    = true
    ssh_helper      = false
  }

  env = {
    GIT_AUTHOR_NAME     = "${data.coder_workspace.me.owner}"
    GIT_COMMITTER_NAME  = "${data.coder_workspace.me.owner}"
    GIT_AUTHOR_EMAIL    = "${data.coder_workspace.me.owner_email}"
    GIT_COMMITTER_EMAIL = "${data.coder_workspace.me.owner_email}"
  }

  metadata {
    display_name = "CPU Usage"
    key          = "0_cpu_usage"
    script       = "coder stat cpu"
    interval     = 10
    timeout      = 1
  }

  metadata {
    display_name = "RAM Usage"
    key          = "1_ram_usage"
    script       = "coder stat mem"
    interval     = 10
    timeout      = 1
  }

  metadata {
    display_name = "Home Disk"
    key          = "3_home_disk"
    script       = "coder stat disk --path $${HOME}"
    interval     = 60
    timeout      = 1
  }

  metadata {
    display_name = "CPU Usage (Host)"
    key          = "4_cpu_usage_host"
    script       = "coder stat cpu --host"
    interval     = 10
    timeout      = 1
  }

  metadata {
    display_name = "Memory Usage (Host)"
    key          = "5_mem_usage_host"
    script       = "coder stat mem --host"
    interval     = 10
    timeout      = 1
  }

  metadata {
    display_name = "Load Average (Host)"
    key          = "6_load_host"
    # get load avg scaled by number of cores
    script   = <<EOT
      echo "`cat /proc/loadavg | awk '{ print $1 }'` `nproc`" | awk '{ printf "%0.2f", $1/$2 }'
    EOT
    interval = 60
    timeout  = 1
  }
}

# ===== Kubernetes Resources =====
resource "kubernetes_persistent_volume_claim" "workspaces" {
  metadata {
    name      = "coder-${lower(data.coder_workspace.me.owner)}-${lower(data.coder_workspace.me.name)}-home"
    namespace = var.namespace
    labels = {
      "coder.owner"                      = data.coder_workspace.me.owner
      "coder.owner_id"                   = data.coder_workspace.me.owner_id
      "coder.workspace_id"               = data.coder_workspace.me.id
      "coder.workspace_name_at_creation" = data.coder_workspace.me.name

      "app.kubernetes.io/name"     = "coder-pvc"
      "app.kubernetes.io/instance" = "coder-pvc-${lower(data.coder_workspace.me.owner)}-${lower(data.coder_workspace.me.name)}"
      "app.kubernetes.io/part-of"  = "coder"
      //Coder-specific labels.
      "com.coder.resource"       = "true"
      "com.coder.workspace.id"   = data.coder_workspace.me.id
      "com.coder.workspace.name" = data.coder_workspace.me.name
      "com.coder.user.id"        = data.coder_workspace.me.owner_id
      "com.coder.user.username"  = data.coder_workspace.me.owner
    }
    annotations = {
      "com.coder.user.email" = data.coder_workspace.me.owner_email
    }
  }
  wait_until_bound = true
  spec {
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "${data.coder_parameter.home_disk_size.value}Gi"
      }
    }
  }
  lifecycle {
    ignore_changes = all
  }
}

resource "kubernetes_deployment" "workspace" {
  depends_on = [kubernetes_persistent_volume_claim.workspaces]

  metadata {
    name      = lower("coder-${data.coder_workspace.me.owner}-${data.coder_workspace.me.name}")
    namespace = var.namespace
    labels = {
      "coder.owner"          = data.coder_workspace.me.owner
      "coder.owner_id"       = data.coder_workspace.me.owner_id
      "coder.workspace_id"   = data.coder_workspace.me.id
      "coder.workspace_name" = data.coder_workspace.me.name

      "app.kubernetes.io/name"     = "coder-workspace"
      "app.kubernetes.io/instance" = lower("coder-workspace-${lower(data.coder_workspace.me.owner)}-${data.coder_workspace.me.name}")
      "app.kubernetes.io/part-of"  = "coder"
      "com.coder.resource"         = "true"
      "com.coder.workspace.id"     = data.coder_workspace.me.id
      "com.coder.workspace.name"   = data.coder_workspace.me.name
      "com.coder.user.id"          = data.coder_workspace.me.owner_id
      "com.coder.user.username"    = data.coder_workspace.me.owner
    }
    annotations = {
      "com.coder.user.email" = data.coder_workspace.me.owner_email
    }
  }
  spec {
    replicas = data.coder_workspace.me.start_count
    selector {
      match_labels = {
        "coder.workspace_id" = data.coder_workspace.me.id
      }
    }
    template {
      metadata {
        labels = {
          "coder.workspace_id" = data.coder_workspace.me.id
        }
      }
      spec {
        # node_selector = {
        #   "karpenter.sh/capacity-type" = "spot"
        # }

        # toleration {
        #   effect   = "NoSchedule"
        #   key      = "workloadspot"
        #   operator = "Equal"
        #   value    = "yes"
        # }

        init_container {
          name              = "init-ide"
          image             = module.jetbrains_gateway.docker_image
          image_pull_policy = "Always"
          command           = ["sh", "-c", "cp -R -n /opt/ide/ /home/coder/.ide"]
          resources {
            requests = {
              "cpu"    = "500m"
              "memory" = "1024Mi"
            }
          }
          volume_mount {
            name       = "workspace-vol"
            mount_path = "/home/coder"
            read_only  = false
          }
        }

        init_container {
          name              = "init-coder-server"
          image             = var.code_server_image
          image_pull_policy = "Always"
          command           = ["sh", "-c", "cp -R -n /code-server /home/coder/.code-server"]
          resources {
            requests = {
              "cpu"    = "500m"
              "memory" = "1024Mi"
            }
          }
          volume_mount {
            name       = "workspace-vol"
            mount_path = "/home/coder"
            read_only  = false
          }
        }

        container {
          name              = "ide"
          image             = data.coder_parameter.image.value == "custom" ? data.coder_parameter.custom_image.value : data.coder_parameter.image.value
          image_pull_policy = "Always"
          command           = ["sh", "-c", coder_agent.main.init_script]
          env {
            name  = "CODER_AGENT_TOKEN"
            value = coder_agent.main.token
          }
          resources {
            requests = {
              "cpu"    = "250m"
              "memory" = "512Mi"
            }
            limits = {
              "cpu"    = "${data.coder_parameter.cpu.value}"
              "memory" = "${data.coder_parameter.memory.value}Gi"
            }
          }

          volume_mount {
            name       = "workspace-vol"
            mount_path = "/home/coder"
            read_only  = false
          }
        }

        volume {
          name = "workspace-vol"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.workspaces.metadata.0.name
          }
        }

        affinity {
          // This affinity attempts to spread out all workspace pods evenly across
          // nodes.
          pod_anti_affinity {
            preferred_during_scheduling_ignored_during_execution {
              weight = 1
              pod_affinity_term {
                topology_key = "kubernetes.io/hostname"
                label_selector {
                  match_expressions {
                    key      = "app.kubernetes.io/name"
                    operator = "In"
                    values   = ["coder-workspace"]
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
