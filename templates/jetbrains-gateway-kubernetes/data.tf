data "coder_provisioner" "me" {}

data "coder_workspace" "me" {}

data "coder_parameter" "image" {
  name        = "Image"
  icon        = "/icon/docker.png"
  default     = "mcr.microsoft.com/vscode/devcontainers/java:17"
  description = "See [Development Container Images](https://hub.docker.com/_/microsoft-vscode-devcontainers) if you don't know where to start"
  type        = "string"
  order       = 2
  mutable     = true

  option {
    name        = "Lumos DevContainer - Java 17"
    description = "https://hub.docker.com/r/lumosdang/devcontainers/tags"
    value       = "${var.private_docker_registry}lumosdang/devcontainers:java-0-17"
  }
  option {
    name        = "Lumos DevContainer - .Net Core 6"
    description = "https://hub.docker.com/r/lumosdang/devcontainers/tags"
    value       = "${var.private_docker_registry}lumosdang/devcontainers:dotnet-6.0-bulleye"
  }
  option {
    name        = "Lumos DevContainer - DevOps"
    description = "https://hub.docker.com/r/lumosdang/devcontainers/tags"
    value       = "${var.private_docker_registry}lumosdang/devcontainers:devops-bullseye"
  }
  option {
    name        = "Microsoft DevContainer - Java 17"
    description = "https://github.com/microsoft/vscode-dev-containers/tree/main/containers/java"
    value       = "mcr.microsoft.com/vscode/devcontainers/java:17"
  }
  option {
    name  = "Custom"
    value = "custom"
  }
}

data "coder_parameter" "custom_image" {
  name        = "Custom Image"
  icon        = "/icon/docker.png"
  default     = "mcr.microsoft.com/vscode/devcontainers/java:17"
  description = "See [Development Container Images](https://hub.docker.com/_/microsoft-vscode-devcontainers) if you don't know where to start"
  type        = "string"
  order       = 3
  validation {
    regex = "[a-z]+"
    error = "Image cannot be left blank!"
  }
  mutable = true
}

data "coder_parameter" "cpu" {
  name         = "cpu"
  display_name = "CPU"
  description  = "The number of CPU cores"
  default      = "4"
  icon         = "/icon/memory.svg"
  mutable      = true
  order        = 4
  option {
    name  = "4 Cores"
    value = "4"
  }
  option {
    name  = "6 Cores"
    value = "6"
  }
  option {
    name  = "8 Cores"
    value = "8"
  }
  option {
    name  = "16 Cores"
    value = "16"
  }
}

data "coder_parameter" "memory" {
  name         = "memory"
  display_name = "Memory"
  description  = "The amount of memory in GB"
  default      = "4"
  icon         = "/icon/memory.svg"
  mutable      = true
  order        = 5
  option {
    name  = "4 GB"
    value = "4"
  }
  option {
    name  = "8 GB"
    value = "8"
  }
  option {
    name  = "16 GB"
    value = "16"
  }
  option {
    name  = "32 GB"
    value = "32"
  }
}

// Immutable data
data "coder_parameter" "repo_url" {
  name         = "repo_url"
  display_name = "Repository URL"
  order        = 1
  default      = ""
  description  = "Optionally enter a custom repository URL, see [awesome-devcontainers](https://github.com/manekinekko/awesome-devcontainers)."
  mutable      = false
}

data "coder_parameter" "repo_branch" {
  name         = "repo_branch"
  display_name = "Repository Branch"
  order        = 2
  default      = "main"
  description  = "Repository Branch used to checkout. Default: main"
  mutable      = false
}

data "coder_parameter" "home_disk_size" {
  name         = "home_disk_size"
  display_name = "Home disk size"
  description  = "The size of the home disk in GB"
  default      = "20"
  type         = "number"
  icon         = "/emojis/1f4be.png"
  mutable      = false
  order        = 3
  validation {
    min = 1
    max = 99999
  }
}
