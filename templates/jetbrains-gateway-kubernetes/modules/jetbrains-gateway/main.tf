locals {
  jetbrains_ides = {
    "GO" = {
      icon = "/icon/goland.svg",
      name = "GoLand",
      # Ref Build Number & Version: https://www.jetbrains.com/go/download/other.html
      value = jsonencode(["GO", "241.14494.238", "/home/coder/.ide", "${var.private_docker_registry}lumosdang/jetbrains-backend:goland-2024.1.4"])
    },
    "IU" = {
      icon = "/icon/intellij.svg",
      name = "IntelliJ IDEA Ultimate",
      # Ref Build Number & Version: https://www.jetbrains.com/idea/download/other.html
      value = jsonencode(["IU", "241.14494.240", "/home/coder/.ide", "${var.private_docker_registry}lumosdang/jetbrains-backend:ideaIU-2024.1.4"])
    },
    "IC" = {
      icon = "/icon/intellij.svg",
      name = "IntelliJ IDEA Community",
      # Ref Build Number & Version: https://www.jetbrains.com/idea/download/other.html
      value = jsonencode(["IC", "241.14494.240", "/home/coder/.ide", "${var.private_docker_registry}lumosdang/jetbrains-backend:ideaIC-2024.1.4"])
    },
    "RD" = {
      icon = "/icon/rider.svg",
      name = "Rider",
      # Ref Build Number & Version: https://www.jetbrains.com/rider/download/other.html
      value = jsonencode(["RD", "241.14494.325", "/home/coder/.ide", "${var.private_docker_registry}lumosdang/jetbrains-backend:rider-2024.1.4"])
    },
    "WS" = {
      icon  = "/icon/webstorm.svg",
      name  = "WebStorm",
      # Ref Build Number & Version: https://www.jetbrains.com/webstorm/download/other.html
      value = jsonencode(["WS", "241.15989.47", "/home/coder/.ide", "${var.private_docker_registry}lumosdang/jetbrains-backend:webstorm-2024.1.5"]),
    },
    "PY" = {
      icon  = "/icon/pycharm.svg",
      name  = "PyCharm Professional",
      value = jsonencode(["PY", "232.9559.58", "https://download.jetbrains.com/python/pycharm-professional-2023.2.1.tar.gz"])
    },
    "PC" = {
      icon  = "/icon/pycharm.svg",
      name  = "PyCharm Community",
      value = jsonencode(["PC", "232.9559.58", "https://download.jetbrains.com/python/pycharm-community-2023.2.1.tar.gz"])
    },
    "CL" = {
      icon  = "/icon/clion.svg",
      name  = "CLion",
      value = jsonencode(["CL", "232.9921.42", "https://download.jetbrains.com/cpp/CLion-2023.2.2.tar.gz"])
    },
    "DB" = {
      icon  = "/icon/datagrip.svg",
      name  = "DataGrip",
      value = jsonencode(["DB", "232.9559.28", "https://download.jetbrains.com/datagrip/datagrip-2023.2.1.tar.gz"])
    },
    "PS" = {
      icon  = "/icon/phpstorm.svg",
      name  = "PhpStorm",
      value = jsonencode(["PS", "232.9559.64", "https://download.jetbrains.com/webide/PhpStorm-2023.2.1.tar.gz"])
    },
    "RM" = {
      icon  = "/icon/rubymine.svg",
      name  = "RubyMine",
      value = jsonencode(["RM", "232.9921.48", "https://download.jetbrains.com/ruby/RubyMine-2023.2.2.tar.gz"])
    }
  }
}

resource "coder_app" "gateway" {
  agent_id     = var.agent_id
  display_name = data.coder_parameter.jetbrains_ide.option[index(data.coder_parameter.jetbrains_ide.option.*.value, data.coder_parameter.jetbrains_ide.value)].name
  slug         = "gateway"
  icon         = data.coder_parameter.jetbrains_ide.option[index(data.coder_parameter.jetbrains_ide.option.*.value, data.coder_parameter.jetbrains_ide.value)].icon
  external     = true
  url = join("", [
    "jetbrains-gateway://connect#type=coder",
    "&workspace=",
    data.coder_workspace.me.name,
    "&agent=",
    var.agent_name,
    "&folder=",
    var.folder,
    "&projectPath=",
    var.folder,
    "&url=${var.ide_public_url}",
    "&token=",
    "$SESSION_TOKEN",
    "&ide_product_code=",
    jsondecode(data.coder_parameter.jetbrains_ide.value)[0],
    # "&ide_build_number=",
    # jsondecode(data.coder_parameter.jetbrains_ide.value)[1],
    "&ide_path_on_host=",
    jsondecode(data.coder_parameter.jetbrains_ide.value)[2],
    "&deploy=false",
    "&newUi=${var.use_new_ui}"
  ])
}

