data "coder_parameter" "jetbrains_ide" {
  type         = "list(string)"
  name         = "jetbrains_ide"
  display_name = "JetBrains IDEs"
  icon         = "/icon/gateway.svg"
  mutable      = true
  # check if default is in the jet_brains_ides list and if it is not empty or null otherwise set it to null
  default = var.default != null && var.default != "" && contains(var.jetbrains_ides, var.default) ? local.jetbrains_ides[var.default].value : local.jetbrains_ides[var.jetbrains_ides[0]].value

  dynamic "option" {
    for_each = { for key, value in local.jetbrains_ides : key => value if contains(var.jetbrains_ides, key) }
    content {
      icon        = option.value.icon
      name        = option.value.name
      value       = option.value.value
    }
  }
}

data "coder_workspace" "me" {}
