output "jetbrains_ides" {
  value = data.coder_parameter.jetbrains_ide.value
}

output "docker_image" {
  value = jsondecode(data.coder_parameter.jetbrains_ide.value)[3]
}

output "ide_path_on_host" {
  value = jsondecode(data.coder_parameter.jetbrains_ide.value)[2]
}