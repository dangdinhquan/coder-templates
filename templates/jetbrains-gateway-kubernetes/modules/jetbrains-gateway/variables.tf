variable "agent_id" {
  type        = string
  description = "The ID of a Coder agent."
}

variable "agent_name" {
  type        = string
  description = "The name of a Coder agent."
}

variable "folder" {
  type        = string
  description = "The directory to open in the IDE. e.g. /home/coder/project"
}

variable "default" {
  default     = ""
  type        = string
  description = "Default IDE"
}

variable "private_docker_registry" {
  type        = string
  default     = ""
  description = "Private Docker Registry used to pull workspace image. Eg: private.registry.com/"
}

variable "ide_public_url" {
  type        = string
  description = "IDE Public URL. Eg: https://ide.yourdomain.com/"
}

variable "jetbrains_ides" {
  type        = list(string)
  description = "The list of IDE product codes."
  validation {
    condition = (
      alltrue([
        for code in var.jetbrains_ides : contains(["IU", "IC", "PS", "WS", "PY", "PC", "CL", "GO", "DB", "RD", "RM"], code)
      ])
    )
    error_message = "The jetbrains_ides must be a list of valid product codes. Valid product codes are: IU, IC, PS, WS, PY, PC, CL, GO, DB, RD, RM."
  }
  # check if the list is empty
  validation {
    condition     = length(var.jetbrains_ides) > 0
    error_message = "The jetbrains_ides must not be empty."
  }
  # check if the list contains duplicates
  validation {
    condition     = length(var.jetbrains_ides) == length(toset(var.jetbrains_ides))
    error_message = "The jetbrains_ides must not contain duplicates."
  }
}

variable "use_new_ui" {
  type = bool
  description = "Use Jetbrains New UI"
  default = true
}
