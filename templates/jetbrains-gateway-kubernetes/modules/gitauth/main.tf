data "coder_parameter" "git_auth_options" {
  name         = "git_auth_options"
  display_name = "Git Authentication"
  description  = "Select Git Authentication Method"
  default      = "gitlab"
  icon         = "/icon/memory.svg"
  mutable      = true
  order        = 1
  option {
    name  = "Gitlab"
    value = "gitlab"
  }
  option {
    name  = "Unauthenticated"
    value = "unauthenticated"
  }
}

# Ref: https://registry.terraform.io/providers/coder/coder/latest/docs/data-sources/git_auth
data "coder_external_auth" "gitlab" {
  count = data.coder_parameter.git_auth_options.value == "gitlab" ? 1 : 0
  # Matches the ID of the git auth provider in Coder.
  id = var.gitlab_auth_id
}
