resource "coder_app" "vscode_desktop" {
  agent_id     = coder_agent.main.id
  external     = true
  icon         = "/icon/code.svg"
  slug         = "vscode-desktop"
  display_name = "VS Code Desktop"
  url = join("", [
    "vscode://coder.coder-remote/open?owner=",
    data.coder_workspace.me.owner,
    "&workspace=",
    data.coder_workspace.me.name,
    "&folder=/home/coder/workspaces/${local.folder_name}",
    "&token=$SESSION_TOKEN",
    ])
}

resource "coder_app" "vscode-browser" {
  depends_on = [coder_agent.main]

  agent_id     = coder_agent.main.id
  slug         = "code-server"
  display_name = "VS Code Browser"
  url          = "http://localhost:13337/?folder=/home/coder/workspaces/${local.folder_name}"
  icon         = "/icon/code.svg"
  subdomain    = false
  share        = "owner"

  healthcheck {
    url       = "http://localhost:13337/healthz"
    interval  = 5
    threshold = 6
  }
}