variable "use_kubeconfig" {
  type        = bool
  description = <<-EOF
  Use host kubeconfig? (true/false)

  Set this to false if the Coder host is itself running as a Pod on the same
  Kubernetes cluster as you are deploying workspaces to.

  Set this to true if the Coder host is running outside the Kubernetes cluster
  for workspaces.  A valid "~/.kube/config" must be present on the Coder host.
  EOF
  default     = false
}

variable "namespace" {
  type        = string
  default     = "development"
  description = "The Kubernetes namespace to create workspaces in (must exist prior to creating workspaces). Default: coder"
}

variable "private_docker_registry" {
  type        = string
  default     = ""
  description = "Private Docker Registry used to pull workspace image. Eg: private.registry.com/"
}

variable "enable_git_auth" {
  type        = bool
  default     = false
  description = "Enable Git Authentication Method Selection."
}

variable "gitlab_auth_id" {
  type    = string
  default = "gitlab"
}

variable "coder_url" {
  type    = string
  default = "http://coder.development.svc.cluster.local"
}

variable "code_server_image" {
  type        = string
  default     = "lumosdang/code-server:4.93.1"
}