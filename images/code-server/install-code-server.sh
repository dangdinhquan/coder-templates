#!/bin/sh

set -e

CODE_SERVER_VERSION=4.93.1
INSTALL_FOLDER=/opt/code-server

mkdir -p ~/.cache/code-server
curl -#fL -o ~/.cache/code-server/code-server-${CODE_SERVER_VERSION}-linux-amd64.tar.gz.incomplete -C - https://github.com/coder/code-server/releases/download/v${CODE_SERVER_VERSION}/code-server-${CODE_SERVER_VERSION}-linux-amd64.tar.gz
mv ~/.cache/code-server/code-server-${CODE_SERVER_VERSION}-linux-amd64.tar.gz.incomplete ~/.cache/code-server/code-server-${CODE_SERVER_VERSION}-linux-amd64.tar.gz

mkdir -p ${INSTALL_FOLDER}/lib ${INSTALL_FOLDER}/bin
tar -C ${INSTALL_FOLDER}/lib -xzf ~/.cache/code-server/code-server-${CODE_SERVER_VERSION}-linux-amd64.tar.gz
mv ${INSTALL_FOLDER}/lib/code-server-${CODE_SERVER_VERSION}-linux-amd64 /code-server