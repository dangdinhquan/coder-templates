### IntelliJ IDEA Ultimate

- File: `Dockerfile.ideaIU`

- Docker Image Tag: `lumosdang/jetbrains-backend:ideaIU-$VERSION`

### IntelliJ IDEA Community

- File: `Dockerfile.ideaIC`

- Docker Image Tag: `lumosdang/jetbrains-backend:ideaIC-$VERSION`

### GoLand

- File: `Dockerfile.goland`

- Docker Image Tag: `lumosdang/jetbrains-backend:goland-$VERSION`

### Rider

- File: `Dockerfile.rider`

- Docker Image Tag: `lumosdang/jetbrains-backend:rider-$VERSION`

