GOLAND=2024.1.4
docker build . -t lumosdang/jetbrains-backend:goland-${GOLAND} -f Dockerfile.goland
docker push lumosdang/jetbrains-backend:goland-${GOLAND}
docker rmi -f lumosdang/jetbrains-backend:goland-${GOLAND}

IDEAIC=2024.1.4
docker build . -t lumosdang/jetbrains-backend:ideaIC-${IDEAIC} -f Dockerfile.ideaIC
docker push lumosdang/jetbrains-backend:ideaIC-${IDEAIC}
docker rmi -f lumosdang/jetbrains-backend:ideaIC-${IDEAIC}

IDEAIU=2024.1.4
docker build . -t lumosdang/jetbrains-backend:ideaIU-${IDEAIU} -f Dockerfile.ideaIU
docker push lumosdang/jetbrains-backend:ideaIU-${IDEAIU}
docker rmi -f lumosdang/jetbrains-backend:ideaIU-${IDEAIU}

RIDER=2024.1.4
docker build . -t lumosdang/jetbrains-backend:rider-${RIDER} -f Dockerfile.rider
docker push lumosdang/jetbrains-backend:rider-${RIDER}
docker rmi -f lumosdang/jetbrains-backend:rider-${RIDER}

WEBSTORM=2024.1.5
docker build . -t lumosdang/jetbrains-backend:webstorm-${WEBSTORM} -f Dockerfile.webstorm
docker push lumosdang/jetbrains-backend:webstorm-${WEBSTORM}
docker rmi -f lumosdang/jetbrains-backend:webstorm-${WEBSTORM}